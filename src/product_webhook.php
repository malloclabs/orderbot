<?php

require 'stripe.inc.php';

define('DATADIR', '/data/products');

$input = @file_get_contents('php://input');
$event = json_decode($input);

assert($event->object == "event");
error_log("Received webhook from stripe:  $event->type");

switch ($event->type) {
	case "product.created":
	case "product.updated":
	case "product.deleted":
		$product_id = $event->data->object->id;
		break;

	case "sku.created":
	case "sku.updated":
	case "sku.deleted":
		$product_id = $event->data->object->product;
		break;

	default:
		error_log("Unknown webhook event:  $event->type");
		http_response_code(500);
}


# Refresh the product and its SKUs
error_log("Refreshing product '${product_id}'...");
$filename = DATADIR . "/${product_id}.json";
if (explode('.', $event->type)[1] == 'deleted') {
	error_log("  (removing '$filename')");
	unlink($filename);
} else {
	$product = stripe("/products/${product_id}");
	$skus = stripe("/skus?product=${product_id}&limit=100");
	$product['skus'] = $skus['data'];

	# Dump it into the data directory
	error_log("  (creating/updating '$filename')");
	file_put_contents($filename, json_encode($product, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
}

# All good!
http_response_code(200);
