<?php

require 'slack.inc.php';

define('PAPERFORM_TOKEN', '3a3e8d9f2c5b33102653ed9db9e6f718');


$token = $_GET['token'];
if ($token != PAPERFORM_TOKEN) {
	http_response_code(403);
	exit("Forbidden!");
}

$order_data = json_decode(file_get_contents('php://input'), true);
$body_pretty = print_r($order_data, true);

# We want to extract the following fields:
#
#		Name			bj7f0
#		Address			8b5nf
#		Suburb			91u7j
#		Total
#		Payment Status

$fieldmap = array(
	'baieh' => 'name',
	'd4fpk' => 'address',
	'a2gid' => 'suburb',
	'cmce2' => 'email',
	'3asl6' => 'phone',
	'28pif' => 'notes',
);
$order = array();
foreach ($order_data['data'] as $field) {
	$order[$fieldmap[$field['key']]] = trim(preg_replace('/\s+/', ' ', $field['value']));
}

$payment_total = sprintf('$%.2f', $order_data['charge']['charge']['amount']/100.0);
$payment_status = $order_data['charge']['charge']['outcome']['type'];
$order_items = array();
foreach ($order_data['charge']['products'] as $sku => $line) {
	$sku = sprintf('%6s', $sku);	
	$order_items[] = "`$sku`  $line[summary]";
}
$order_count = count($order_items);

# Link to the Stripe dashboard
$is_live = $order_data['charge']['charge']['livemode'] ? '*LIVE*' : '*TEST MODE*';
$stripe_mode = $order_data['charge']['charge']['livemode'] ? 'payments' : 'test/payments';
$stripe_id = $order_data['charge']['charge']['id'];
$stripe_link = "https://dashboard.stripe.com/${stripe_mode}/${stripe_id}";



# Generate the message attachment
$msgfields[] = array('title' => 'Customer', 'short' => true, 'value' => "_$order[name]_\n<tel:$order[phone]|$order[phone]>\n$order[email]");
$msgfields[] = array('title' => 'Delivery', 'short' => true, 'value' => "$order[address]\n_$order[suburb]_");
$msgfields[] = array('title' => 'Payment', 'short' => true, 'value' => "$payment_total  `${payment_status}`\n${is_live}\n<${stripe_link}|View charge info>");
$msgfields[] = array('title' => 'Products', 'value' => join("\n", $order_items));
if (!empty($order['notes'])) {
	$msgfields[] = array('title' => 'Order Notes', 'value' => $order['notes']);
}

$footer[] = 'Submission device: *' . $order_data['device']['platform'] . ' (' . $order_data['device']['type'] . ')*';



$summary = "New order from '$order[name]' for $payment_total ($order_count items).";


slack_post(SLACK_TOKEN_OPS, array(
	'attachments' => array(array(
		'mrkdwn_in' => array('footer', 'fields'),
		'fields' => $msgfields,
		'pretext' => $summary,
		'fallback' => $summary,
		'color' => 'good',
		'footer' => join("\n", $footer),
	)),
));

