<?php

define('STRIPE_API_KEY', 'rk_test_HbyLvSHgF5hu6ECpbYZXWmT6');

function stripe($path, $body = null) {
	$req = curl_init('https://api.stripe.com/v1' . $path);	
	$headers = array();
	curl_setopt($req, CURLOPT_USERPWD, STRIPE_API_KEY . ':');
	curl_setopt($req, CURLOPT_RETURNTRANSFER, 1);
	if (!empty($body)) {
		curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($body));
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
	}
	curl_setopt($req, CURLOPT_HTTPHEADER, $headers);
	$resp = curl_exec($req);
	if (!$resp) {
		error_log("Error with request for '${path}': " . curl_error($req));
	}
	curl_close($req);
	return json_decode($resp, true);
}
