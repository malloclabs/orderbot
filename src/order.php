<?php

define('DATADIR', '/data/products');

$products = array();
foreach (glob(DATADIR . '/prod_*.json') as $f) {
	$data = json_decode(file_get_contents($f), true);
	$category = $data['metadata']['category'];
	$products[$category][] = $data;
}

function attr($value) {
	return '"' . htmlspecialchars($value) . '"';
}

function field($name, $label, array $options = array()) {
	$type = $options['type'] || 'text';
	$classes = 'input';
	if ($options['class']) {
		$classes .= ' ' . $options['class'];
	}
	echo '<div class="field is-horizontal">';
	echo '<div class="field-label is-normal">';
	echo '  <label class="label" for=', attr("field-$name"), '>';
	echo htmlspecialchars($label);
	echo '  </label>';
	echo '</div>';
	echo '<div class="field-body">';
	echo '  <div class="field">';
	echo '    <p class="control">';
	echo '      <input';
		echo ' class=', attr($classes);
		echo ' type=', attr($type);
		echo ' name=', attr($name);
		echo ' id=', attr("field-$name");
		if ($options['readonly']) {
			echo ' readonly';
		}
	echo '>';
	echo '	  </p>';
	echo '  </div>';
	echo '</div>';
	echo '</div>';
END;
}

function button($name, $label, array $options = array()) {
	echo '<button class="button';
	if ($options['primary']) {
		echo ' is-primary';
	}
	if ($options['as_link']) {
		echo ' is-text';
	}
	echo '" name=', attr($name), ' type="submit">';
	echo htmlspecialchars($label);
	echo '</button>';
}


?>
<!doctype html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles.css?111">
<style>
body {
	font-family: sans-serif;
}
div.total { font-weight: bold; }
div.total:before { content: "Total: "; }
.steps .icon { font-size: 0.7em; }
form > legend { display: none; }
fieldset > legend { display: block; font-size: 1.2em; text-align: center; }
fieldset { border: none; }
</style>
<script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"></script>
<title>Order Form</title>
<body>

  <section class="section">
  <form name="order" action="placeorder.php" method="POST" class="container">
  <legend>Order Form</legend>

  <ul class="steps has-content-centered is-medium">
    <li class="steps-segment">
	  <span class="steps-marker"><span class="icon is-small"><i class="fas fa-shopping-cart"></i></span></span>
	  <div class="steps-content">
		<p class="is-size-6">Shop</p>
		<p class="is-size-7">Select the items that you want.</p>
      </div>
	</li>
	<li class="steps-segment is-active">
	  <span class="steps-marker"><span class="icon is-small"><i class="fas fa-address-card"></i></span></span>
	  <div class="steps-content">
		<p class="is-size-6">Information</p>
		<p class="is-size-7">Tell us where you want them delivered.</p>
      </div>
	</li>
	<li class="steps-segment">
	  <span class="steps-marker"><span class="icon is-small has-text-grey"><i class="fas fa-money-check-alt"></i></span></span>
	  <div class="steps-content">
		<p class="is-size-6">Payment</p>
		<p class="is-size-7">Pay for your purchase using any credit or debit card.</p>
      </div>
	</li>
	<li class="steps-segment">
	  <span class="steps-marker"><span class="icon is-small has-text-grey"><i class="fas fa-shipping-fast"></i></span></span>
	  <div class="steps-content">
		<p class="is-size-6">Delivery</p>
		<p class="is-size-7">...and we'll deliver the items to your door within the next hour!</p>
      </div>
	</li>
  </ul>

  <fieldset id="order1" class="section">
	<legend>Select your products</legend>

	<div class="tabs is-centered is-boxed">
	  <ul>
		<li class="is-active">
		  <a>
			<span class="icon is-small"><i class="far fa-sun" aria-hidden="true"></i></span>
			<span>Milk &amp; Dairy</span>
		  </a>
		</li>
		<li class="">
		  <a>
			<span class="icon is-small"><i class="fas fa-cubes" aria-hidden="true"></i></span>
			<span>Snacks</span>
		  </a>
		</li>
		<li class="">
		  <a>
			<span class="icon is-small"><i class="fas fa-cubes" aria-hidden="true"></i></span>
			<span>Drinks</span>
		  </a>
		</li>
		<li class="">
		  <a>
			<span class="icon is-small"><i class="far fa-lemon" aria-hidden="true"></i></span>
			<span>Fruit</span>
		  </a>
		</li>
		<li class="">
		  <a>
			<span class="icon is-small"><i class="fas fa-cubes" aria-hidden="true"></i></span>
			<span>Essentials</span>
		  </a>
		</li>
	  </ul>
	</div>

  <?php foreach ($products as $cat => $prod_list): ?>
	<?php foreach ($prod_list as $prod): ?>
	  <article class="media product">

		<figure class="media-left">
		  <p class="image is-64x64">
		    <img src="<?= $prod['images'][0] ?>" width="100" height="100">
		  </p>
		</figure>
		<div class="media-content">
		  <p><strong><?= htmlspecialchars($prod['name']); ?></strong></p> 
		  <p>Lorem ipsum dolor sit amet....</p>
		<?php foreach ($prod['skus'] as $sku): ?>
		  <div class="media product-sku">
			<figure class="media-left">
			  <p class="image is-48x48">
			    <img src="<?= $sku['image'] ?>" width="50" height="50">
			  </p>
			</figure>
			<div class="media-content">
			  <span class="attr"><?= htmlspecialchars(implode(', ', $sku['attributes'])); ?></span>
			  (<?= sprintf('$%.2f', $sku['price'] / 100.0) ?>)
			</div>
			<div class="media-right">
			  <input class="qty" type="number" name="item/<?=$sku['id']?>" min="0" max="99" placeholder="Qty" data-price="<?= $sku['price'] ?>">
			</div>
		  </div>
		<?php endforeach; ?>
		</div>
	  </article>
	<?php endforeach; ?>
  <?php endforeach; ?>
  </fieldset>

  <fieldset id="order2" class="section">
	<legend>Your details</legend>
	<?= field("custname", "Name") ?>
	<?= field("custphone", "Mobile", array('type'=>'tel')) ?>
	<?= field("custemail", "E-mail", array('type'=>'email')) ?>
	<?= field("custaddress", "Address") ?>
	<?= field("custsuburb", "Suburb") ?>
  </fieldset>

  <fieldset id="order3" class="section">
	<legend>Confirm order</legend>
	<?= field("total", "Order Total", array('readonly'=>true, 'class'=>'is-static')) ?>
	<div class="field is-grouped is-grouped-right">
	  <p class="control">
		<?= button("cancel", "Cancel", array('as_link' => true)); ?>
	  </p>
	  <p class="control">
		<?= button("order", "Place order", array('primary' => true)); ?>
	  </p>
	</div>
  </fieldset>
  </form>
  </section>

  <!-- some awesome javascriptines... -->
  <script>
  document.addEventListener("DOMContentLoaded", function () {
	var form = document.forms['order'];
	form.addEventListener('input', function () {
		var sum = 0;
		for (var i = 0; i < form.elements.length; i++) {
			var elem = form.elements[i];
			if (elem.classList.contains('qty')) {
				sum += (elem.value * elem.dataset.price);
			}
		}
		form.elements['total'].value = (sum/100).toFixed(2);
	});
  });
  </script>
</body>
