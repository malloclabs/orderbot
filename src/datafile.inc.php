<?php
namespace datafile;


function is_jsident($value) {
	if ($value === 'true' || $value === 'false' || $value === 'null') {
		return FALSE;
	}
	return preg_match('/^[a-zA-Z_][a-zA-Z0-9_]*$/', $value);
}


function do_output($f, $path, &$value, $keys=null) {
	if (is_null($value)) {
		return;
	}
	if (is_scalar($value)) {
		$out = json_encode($value, JSON_UNESCAPED_SLASHES);
		fwrite($f, "${path}: ${out}\n");
		return;
	}
	#$obj = is_object($value) ? get_object_vars($value) : $value;
	foreach ($value as $objkey => $objvalue) {
		if (isset($keys) && !in_array($objkey, $keys)) {
			continue;
		}
		$prefix = $path;
		if (is_integer($objkey)) {
			$prefix .= '[' . $objkey . ']';
		} else if (!is_jsident($objkey)) {
			$prefix .= '[' . json_encode($objkey, JSON_UNESCAPED_SLASHES). ']';
		} else {
			$prefix .= empty($path) ? '' : '.';
			$prefix .= $objkey;
		}
		do_output($f, $prefix, $objvalue);
	}
}


function array_has_only_objects(&$arr) {
	foreach ($arr as $val) {
		if (!is_object($val)) return false;
	}
	return true;
}


function datafile_write($filename, &$obj) {
	assert(is_object($obj));
	foreach ($obj as $key => $value) {
		if (is_array($value)) {
			$sections[] = $key;
		}
	}
	$f = fopen($filename, "w");	
	$normal = array();
	$sections = array();
	foreach ($obj as $key => $value) {
		# XXX this is gross, we should refactor it
		if (is_array($value) && is_jsident($key) && array_has_only_objects($value) && !empty($value)) {
			$sections[] = $key;
		} else {
			$normal[] = $key;
		}
	}
	do_output($f, '', $obj, $normal);
	foreach ($sections as $sect) {
		foreach ($obj->{$sect} as $key => $value) {
			# XXX: if we're outputting a numeric array, just output '% sect[]'
			# instead of '% sect[0]', '% sect[1]', etc...
			$key = json_encode($key, JSON_UNESCAPED_SLASHES);
			fwrite($f, "\n% ${sect}[${key}]\n");
			do_output($f, '', $value);
		}
	}
	fclose($f);
}


# NOTE:  we allow paths that begin with an indexed part, e.g. '[0].foo'
# each path component must begin with:
#	a-zA-Z_		bare identifier
#	[			indexed access
# each indexed access is either:
#	0-9+		array subscript
#	"			string subscript
function parsepath($path) {
	$result = array();
	while (!empty($path)) {
		# chomp whitespace
		$path = ltrim($path);
		# read an identifier
		if (preg_match('/^[a-zA-Z_][a-zA-Z0-9_]*/', $path, $match)) {
			$result[] = $match[0];
			$path = substr($path, strlen($match[0]));
		} else if ($path[0] == '[') {
			# XXX is there a better way to encode this exception?
		} else {
			trigger_error("JSON path must begin with an identifier", E_USER_ERROR);
		}

		# read a series of operators
		while (!empty($path)) {
			# chomp whitespace
			$path = ltrim($path);
			if ($path[0] == '.') {
				$path = substr($path, 1);
				break;			# return to the outer loop to read another ident
			} else if ($path[0] == '[') {
				if (preg_match('/^\[\s*(0|[1-9][0-9]*)\s*\]/', $path, $match)) {
					$result[] = (int)$match[1];
					$path = substr($path, strlen($match[0]));
				} else if (preg_match('/^\[\s*(\"[^\\\"]*\")\s*\]/', $path, $match)) {
					$result[] = json_decode($match[1]);
					$path = substr($path, strlen($match[0]));
				} else {
					trigger_error("Invalid subscript in JSON path", E_USER_ERROR);
				}
			} else {
				trigger_error("Invalid operator '$path[0]' in JSON path", E_USER_ERROR);
			}
		}
	}
	return $result;
}


function &datafile_read($filename) {
	$obj = (object)array();
	$prefix = [];
	$f = fopen($filename, "r");
	while (($line = fgets($f)) !== false) {
		$line = trim($line);
		if (empty($line) || $line[0] == '#') {
			continue;
		}
		if ($line[0] == '%') {
			$prefix = parsepath(substr($line, 1));
			continue;
		}
		# FIXME: parse the path (including embedded quoted strings), and only then look
		# for the ':' that splits the key from the value...
		$item = preg_split('/:\s*/', $line, 2);
		$key = parsepath($item[0]);

		# set the value using the obtained path
		$value_ref = &$obj;
		$path = array_merge($prefix, $key);
		foreach ($path as $k) {
			if (is_integer($k) || ctype_digit($k)) {
				$value_ref = &$value_ref[$k];
			} else {
				$value_ref = &$value_ref->{$k};
			}
		}
		$value_ref = json_decode($item[1]);
	}

	fclose($f);
	return $obj;
}
