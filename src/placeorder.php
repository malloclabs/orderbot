<?php

require 'stripe.inc.php';

?>



<!doctype html>
<h1>Order API</h1>

<?php
# TODO: validate the bloody inputs

$items = [];
foreach ($_POST as $key => $value) {
	$keys = explode('/', $key);
	echo "key: <b>$key</b><br>";
	if ($keys[0] == 'item' && $value > 0) {
		$sku = $keys[1];
		echo "(adding item...)<br>";
		$items[] = [
			'type' => 'sku',
			'parent' => $sku,
			'quantity' => $value,
			'amount' => 31337,
		];
	}
}

# TODO: we should find a way to pass the 'total' form field (if any)
# so that Stripe can validate our pricing calculations...

$order_req = [
	'currency' => 'NZD',
	'email' => $_POST['custemail'],
	'items' => $items,
	'metadata' => ['foo' => 'TODO'],
	'shipping' => [
		'name' => $_POST['custname'],
		'phone' => $_POST['custphone'],
		'address' => [
			'line1' => $_POST['custaddress'],
			'line2' => $_POST['custsuburb'],
			'city' => 'Wellington',
			'country' => 'New Zealand',
		],
	],
];

echo '<h2>API request</h2><pre>';
echo htmlspecialchars(http_build_query($order_req));
echo '</pre>';

$order_resp = stripe('/orders', $order_req);


echo '<h2>API result</h2><pre>';
echo htmlspecialchars(print_r($order_resp, true));
echo '</pre>';
